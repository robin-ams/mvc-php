<?php
class Autoloader
{
    public static function loader($class)
    {
        $core_path = ROOT.DS.'app'.DS.'core'.DS.strtolower($class).'.class.php';
        $controller_path = ROOT.DS.'app'.DS.'controllers'.DS.str_replace('controller','',strtolower($class)).'.controller.php';
        $model_path = ROOT.DS.'app'.DS.'models'.DS.strtolower($class).'.php';

        if(file_exists($core_path)) {
            require_once($core_path);
        }
        elseif (file_exists($controller_path)) {
            require_once($controller_path);
        }
        elseif (file_exists($model_path)) {
            require_once($model_path);
        }
        else {
            throw new Exception("Error loading class: ".$class);
        }
    }
}
?>
