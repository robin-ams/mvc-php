<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(dirname(__FILE__)));
define('VIEWS_PATH', ROOT.DS.'app'.DS.'views');

require_once(ROOT.DS.'app'.DS.'core'.DS.'init.php');

$router = new Router($_SERVER['REQUEST_URI']);



App::run($_SERVER['REQUEST_URI']);

/*

echo "<pre>";

print_r('Route: '.$router->getRoute().PHP_EOL);
print_r('Language: '.$router->getLanguage().PHP_EOL);
print_r('Controller: '.$router->getController().PHP_EOL);
print_r('Action: '.$router->getMethodPrefix().$router->getAction().PHP_EOL);

echo "Params: ";
print_r($router->getParams());

*/
 ?>
